# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# t.string :store
# t.string :name
# t.string :description
# t.integer :price
# t.integer :quantity
# t.string :category

Product.create(store: "Tiendus", name: "Camisa", description: "Camisa Bonita", price: rand(100), quantity: rand(20), category: "clothes");
Product.create(store: "Tiendus", name: "Pantalon pegadito", description: "Pantalon sexy de encaje ", price: rand(100), quantity: rand(20), category: "clothes");
Product.create(store: "Tiendus", name: "Hilo de encaje colombiano", description: "sexy hilo de encaje con brillo sedoso ", price: rand(100), quantity: rand(20), category: "clothes");