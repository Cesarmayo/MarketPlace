Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  resources :products , only: [:index, :new, :update, :destroy, :create, :show, :edit]
  resources :affiliates, only: [:index, :new, :update, :destroy, :create, :show, :edit]

end
