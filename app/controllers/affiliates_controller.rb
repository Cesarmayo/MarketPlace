class AffiliatesController < ApplicationController
    skip_before_action :verify_authenticity_token

  def index
    @affiliate = Affiliate.query
    render json: @affiliate
  end

  def new
    @affiliate = Affiliate.new
  end

  def create
    @affiliate = Affiliate.new(affiliate_params)
    if @affiliate.save
      puts "hola"
      redirect_to affiliates_path
    else
      render new, notice: 'no se pudo enviar el fomulario'
    end
  end

private

  def affiliate_params
    params.require(:affiliate).permit(:name,:category, :url)
  end

end 




