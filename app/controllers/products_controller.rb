class ProductsController < ApplicationController

  skip_before_action :verify_authenticity_token
  def index
    @product = Product.all
    #render json: @product
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(product_params)
    if @product.save
      puts "hola"
      redirect_to products_path
    else
      render new, notice: 'no se pudo enviar el fomulario'
    end
  end

  def show
    @product
  end

  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_path, notice: 'el producto ha sido eliminado.' }
    end
  end

  def edit
    @product
  end

  def update
    if @product.update(product_params)
      redirect_to @product
    else
      render 'edit'
    end
  end

  private

  def product_params
    params.require(:product).permit(:store, :name, :description, :price, :quantity, :category)
    # falta inmagen
  end

  def find_product
    @product = Product.find_by(id: params[:id])
  end
end




