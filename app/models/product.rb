# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  store       :string
#  name        :string
#  description :string
#  price       :integer
#  quantity    :integer
#  category    :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Product < ApplicationRecord
    #enum category: {clothes: 0}
end
