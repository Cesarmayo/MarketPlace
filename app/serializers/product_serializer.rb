class ProductSerializer < ActiveModel::Serializer
    attributes :store, :name, :description, :price, :quantity, :category
end